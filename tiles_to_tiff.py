#!/usr/bin/env python

from typing import Optional
from urllib.request import urlopen
from shutil import copyfileobj
import ssl
import os
import glob
import subprocess
import shutil
import argparse
from tile_convert import bbox_to_tilerange, bbox_tile
from osgeo import gdal, osr, ogr
from datetime import datetime
import re
import csv

#---------- CONFIGURATION -----------#
# tile_server =  "https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}"
# tile_server = "https://mt1.google.com/vt/lyrs=s&x={x}&y={y}&z={z}"
work_dir = os.getcwd()
temp_dir = os.path.join(work_dir, 'temp')
output_dir = os.path.join(work_dir, 'output')

#-----------------------------------#

def main():
    parser = argparse.ArgumentParser(description='Download tiles from a tile-server and merge it to a geotiff file.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--radius', nargs=3, metavar=('LATITUDE', 'LONGITUDE', 'RADIUS'),default=[48.395288, 9.9826028, 40_000],
                        help='Adapt Bounding-Box of the Map, so that a radius (in meters) around some center-coordinates is included.')
    parser.add_argument('--bbox', nargs=4, metavar=('LATITUDE_MIN', 'LATITUDE_MAX', 'LONGITUDE_MIN', 'LONGITUDE_MAX'), help='Bounding-Box of map')
    parser.add_argument('--zoom', default="16",type=int,
                        help='zoom-level between 0..19 (max zoomlevel depends on the tile-server')
    parser.add_argument('--tileserver', default="https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}",type=str,
                        help='xyz tileserver used')
    parser.add_argument('--sslverify', default=True, type=lambda x: (str(x).lower() in ['true', '1','yes']), help="Setting this to false disables ssl-verification. This may be used to avoid CERT-Errors when connection through an intercepting ssl-proxy.")
    parser.add_argument('--csv', default='', type=str, help='A csv file (excel) with multiple rows (zoom, latitude, longitude, radius) which are used generate geotiffs for multiple bounding boxes and/or zoom-levels.')

    args = parser.parse_args()
    
    ssl_context: Optional[ssl.SSLContext] =  None
    if(not args.sslverify):
        ssl_context = ssl._create_unverified_context()

    bbox = args.bbox
    if(bbox is None):
        latitude_center, longitude_center, radius = [float(val) for val in args.radius]
        bbox = get_range_circle_envelope(latitude_center, longitude_center, radius)
    
    tile_server = args.tileserver

    if args.csv:
        with open(args.csv, newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                zoom = row["zoom"] 
                latitude_center = row["lat"]
                longitude_center = row["lon"]
                radius = row["radius"]
                bbox = get_range_circle_envelope(float(latitude_center), float(longitude_center), float(radius))
                create_tiff_for_bbox(bbox=bbox, zoom=float(zoom), tile_server=tile_server, ssl_context=ssl_context)
        return
    
    create_tiff_for_bbox(bbox=bbox, zoom=args.zoom, tile_server=tile_server, ssl_context=ssl_context)

def create_tiff_for_bbox(bbox, zoom, tile_server, ssl_context: Optional[ssl.SSLContext] = None):
    lat_min, lat_max, lon_min, lon_max = bbox

    print(f"Downloading and Merging Tiles in BoundingBox ( lat: {str([lat_min, lat_max])}, lon: {str([lon_min,lon_max])} ) at Zoomlevel {zoom} from {tile_server}...")

    x_min, x_max, y_min, y_max = bbox_to_tilerange(lon_min, lon_max, lat_min, lat_max, zoom)

    print(f"Downloading {(x_max - x_min + 1) * (y_max - y_min + 1)} tiles")

    for required_dir in [temp_dir, output_dir]: 
        if not os.path.exists(required_dir):
            os.makedirs(required_dir)

    for x in range(x_min, x_max + 1):
        for y in range(y_min, y_max + 1):
            print(f"Downloading and Georeference Tile {x},{y}")
            png_path = download_tile(x, y, zoom, tile_server, ssl_context = ssl_context)
            georeference_raster_tile(x, y, zoom, png_path)

    print("Download complete")

    date_time = datetime.now().strftime("%Y%m%d%H%M%S")
    filename = f"z{zoom}_lat_{lat_min:.4f}_{lat_max:.4}_lon_{lon_min:.4}_{lon_max:.4}_d{date_time}"
    filename = re.sub('[^0-9a-zA-Z_]+', '_', filename)

    path_merged_file = output_dir + f'/{filename}.tif'
    print(f"Merging tiles into {path_merged_file}")
    if(os.path.isfile(path_merged_file)):
        os.remove(path_merged_file)
    merge_tiles(temp_dir + '/*.tif', path_merged_file)
    print("Merge complete")
    shutil.rmtree(temp_dir)


"""
echo "0 0 0" | gdaltransform -s_srs 'PROJCS["ENU (-77.410692720411:39.4145340892321)",GEOGCS["WGS 84",DATUM["World Geodetic System 1984",SPHEROID["WGS 84",6378137.0,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0.0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.017453292519943295],AUTHORITY["EPSG","4979"]],PROJECTION["Orthographic"],PARAMETER["Latitude_Of_Center",39.4145340892321],PARAMETER["Longitude_Of_Center",-77.410692720411],PARAMETER["false_easting",0],PARAMETER["false_northing",0],UNIT["Meter",1]]' -t_srs EPSG:4326+3855
"""
def get_enu_to_wgs84_transformation(latitude_center=39.4145340892321, longitude_center=-77.410692720411):
    wkt = f"""
        PROJCS["ENU ({longitude_center}:{latitude_center})",
                GEOGCS["WGS 84",
                    DATUM["World Geodetic System 1984",
                        SPHEROID["WGS 84",6378137.0,298.257223563,
                            AUTHORITY["EPSG","7030"]],
                        AUTHORITY["EPSG","6326"]],
                    PRIMEM["Greenwich",0.0, AUTHORITY["EPSG","8901"]],
                    UNIT["degree",0.017453292519943295],
                    AUTHORITY["EPSG","4979"]],
                PROJECTION["Orthographic"],
                PARAMETER["Latitude_Of_Center",{latitude_center}],
                PARAMETER["Longitude_Of_Center",{longitude_center}],
                PARAMETER["false_easting",0],
                PARAMETER["false_northing",0],
                UNIT["Meter",1]]
          """
    src = osr.SpatialReference()
    src.ImportFromWkt(wkt)

    tgt = osr.SpatialReference()
    tgt.ImportFromEPSG(4326)

    transform = osr.CoordinateTransformation(src, tgt)
    return transform


def get_range_circle_envelope(latitude_center=39.4145340892321, longitude_center=-77.410692720411, radius= 40_000):
    east_north_directions = [
        [0, radius], # NORTH
        [radius, 0], # EAST
        [0, -radius], # SOUTH
        [-radius, 0], # WEST
    ]
    wkt_point_list = ', '.join([ 
        f'{en[0]} {en[1]}' for en in east_north_directions 
    ])

    geom = ogr.CreateGeometryFromWkt(f"LINESTRING ({wkt_point_list})")
    # since the bbx is defined in lat/lon (wgs84) we neet to transform the points into that coordinates:
    transform = get_enu_to_wgs84_transformation(latitude_center, longitude_center)
    geom.Transform(transform)
    lat_min, lat_max, lon_min, lon_max = geom.GetEnvelope()
    return [lat_min, lat_max, lon_min, lon_max]


def download_tile(x, y, z, tile_server, ssl_context: Optional[ssl.SSLContext] = None):
    url = tile_server
    for (placeholder, content) in [ ("{x}",x), ("{y}",y), ("{z}",z) ]:
        url = url.replace(placeholder, str(content))
    path = f'{temp_dir}/{x}_{y}_{z}.png'

    with urlopen(url, context=ssl_context) as in_stream, open(path, 'wb') as out_file:
        copyfileobj(in_stream, out_file)
    return(path)


def merge_tiles(input_pattern, output_path):
    merge_command = ['gdal_merge.py', '-o', output_path]
    for name in glob.glob(input_pattern):
        merge_command.append(name)
    subprocess.call(merge_command)


def georeference_raster_tile(x, y, z, path):
    bounds = bbox_tile(x, y, z)
    filename, _extension = os.path.splitext(path)
    gdal.Translate(filename + '.tif',
                   path,
                   outputSRS='EPSG:4326',
                   outputBounds=bounds)


if __name__ == "__main__":
    main()