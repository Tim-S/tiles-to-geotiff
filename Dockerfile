FROM osgeo/gdal:alpine-normal-latest

WORKDIR /data

ADD *.py  /usr/bin/

RUN chmod +x /usr/bin/tiles_to_tiff.py

ENTRYPOINT ["tiles_to_tiff.py"]