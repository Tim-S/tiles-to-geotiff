from math import log, tan, radians, cos, pi, floor, degrees, atan, sinh, asinh

def sec(x):
    return(1/cos(x))


def tilenum_to_degree(xtile, ytile, zoom):
    """
    Tile numbers to lon./lat.
    (see https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames)
    """
    n = 2.0 ** zoom
    lon_deg = xtile / n * 360.0 - 180.0
    lat_rad = atan(sinh(pi * (1 - 2 * ytile / n)))
    lat_deg = degrees(lat_rad)
    return (lat_deg, lon_deg)


def degree_to_tilenum(lat_deg, lon_deg, zoom):
    """
    Lon./lat. to tile numbers.
    (see https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames)
    """
    lat_rad = radians(lat_deg)
    n = 2.0 ** zoom
    xtile = int((lon_deg + 180.0) / 360.0 * n)
    ytile = int((1.0 - asinh(tan(lat_rad)) / pi) / 2.0 * n)
    return (xtile, ytile)


def bbox_tile(xtile, ytile, zoom):
    """
    Return the BoundingBox of a xyz tile in degrees 
    """
    lat_deg_min, lon_deg_min = tilenum_to_degree(xtile, ytile, zoom)
    lat_deg_max, lon_deg_max = tilenum_to_degree(xtile+1, ytile+1, zoom)
    return[lon_deg_min, lat_deg_min, lon_deg_max, lat_deg_max]

def bbox_to_tilerange(lon_min, lon_max, lat_min, lat_max, z):
    x_min, y_max = degree_to_tilenum(lat_min, lon_min, z)
    x_max, y_min = degree_to_tilenum(lat_max, lon_max, z)
    return(x_min, x_max, y_min, y_max)
