# tiles-to-tiff

Python script for converting XYZ raster tiles for slippy maps to a georeferenced TIFF image.

## Build the Docker-Image

```sh
docker build -t schneidertim/tiles_to_tiff:latest .
```

## Run with Docker

```sh
docker run -it --rm  -v ${PWD}:/data schneidertim/tiles_to_tiff:latest --radius 48.395288 9.9826028 500
```

you can also use create a csv-file with multiple coordinates + radius and zoom for creating multiple maps with a single invocation

```sh
docker run -it --rm  -v ${PWD}:/data schneidertim/tiles_to_tiff:latest --csv locations.csv
```

where locations.csv contains a list of locations, like following example:

```csv
lat,lon,radius,zoom
48.395288,9.9826028,4000,14
48.395288,9.9826028,500,16
```
